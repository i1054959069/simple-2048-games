# 简易2048小游戏

#### 介绍
这是一个简单的 2048 游戏，使用原生 Android 平台搭建。通过 TextView 控件和算法实现游戏逻辑。

#### 功能特点
>通过滑动屏幕来移动数字方块。
>相同数字的方块会合并成为一个更大的方块。
>游戏失败条件：没有可以移动的方块。

#### 系统要求
Android 系统版本：4.0.3 或以上。

#### 软件架构
未使用任何框架，纯算法实现

#### 安装说明
1.下载 APK 文件至 Android 设备。
2.在设备上运行 APK 文件以进行安装。



### 游戏截图
![输入图片说明](https://images.gitee.com/uploads/images/2020/1022/103306_7d075b5a_7522400.jpeg "Screenshot_20201022_103136.jpg")

![输入图片说明](https://images.gitee.com/uploads/images/2020/1022/103314_f12d65c6_7522400.jpeg "Screenshot_20201022_103216.jpg")

![输入图片说明](https://images.gitee.com/uploads/images/2020/1022/103327_68a3cdc7_7522400.jpeg "Screenshot_20201022_103202.jpg")